package com.levelUp.storage.entities;

import java.util.UUID;

//import javax.persistence.Entity;
//
//@Entity
public class Customer implements IdEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public UUID getId() {
        return null;
    }
}
