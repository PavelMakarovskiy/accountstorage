package com.levelUp.storage.entities;

import java.util.Date;
import java.util.UUID;

public class Account implements IdEntity {
    private Customer customer;

    private String bank;

    private long accountNumber;

    private long balance;

    private Date dateOfOpeningAccount;

    private Date dateOfClosingAccount;

    private Currency currency;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public Date getDateOfOpeningAccount() {
        return dateOfOpeningAccount;
    }

    public void setDateOfOpeningAccount(Date dateOfOpeningAccount) {
        this.dateOfOpeningAccount = dateOfOpeningAccount;
    }

    public Date getDateOfClosingAccount() {
        return dateOfClosingAccount;
    }

    public void setDateOfClosingAccount(Date dateOfClosingAccount) {
        this.dateOfClosingAccount = dateOfClosingAccount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public UUID getId() {
        return null;
    }
}
