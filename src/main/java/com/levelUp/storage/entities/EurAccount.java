package com.levelUp.storage.entities;

public class EurAccount extends Account {
    public EurAccount() {
        super.setCurrency(Currency.EUR);
    }
}
