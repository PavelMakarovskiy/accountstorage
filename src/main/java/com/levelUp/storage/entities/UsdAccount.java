package com.levelUp.storage.entities;

public class UsdAccount extends Account {
    public UsdAccount() {
        super.setCurrency(Currency.USD);
    }
}
