package com.levelUp.storage.entities;

import java.util.UUID;

public interface IdEntity {
    UUID getId();
}
