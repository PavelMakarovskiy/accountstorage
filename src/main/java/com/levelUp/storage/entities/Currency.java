package com.levelUp.storage.entities;

public enum Currency {
    RUR,
    USD,
    EUR
}
